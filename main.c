/* 
  File:   main.c
  Author:  rfurch
 
  TODO:
  - revisar que pasa si se cambia el tipo de acceso en BD (el proceso podria suicidarse!)  	
  - ver el funcionamiento a largo plazo tanto del lado cliente como servidor (router)
*/


#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <time.h>
#include <pty.h>
#include <sys/time.h>
#include <sys/select.h>
#include <stdio.h>
#include <ctype.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <semaphore.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <signal.h>
#include <pty.h>
#include <math.h>
#include <mysql/mysql.h>

#include <sys/timeb.h>

#include "ifaceData.h"                                                                                                     

int             _verbose=0;
pid_t           _child_pid=0;
int             _to_epics=0;
int             _to_files=0;
int             _to_memdb=0;
int             _to_hisdb=0;
int             _send_alarm=0;                                         
char            _process_name[100];
int 			_sample_period=10;   // default sample time in seconds
int 			_reconnect_period=10;   // default reconnec time in minutes

int				_speech_prio=8;		// minimum priority value to trigger speech messages

MYSQL       	*_mysql_connection_handler=NULL;


extern char     server[];

//------------------------------------------------------------------------                                   
//------------------------------------------------------------------------

void printUsage(char *prgname) //nada
{
printf ("============================================================\n");
printf ("Trend file generator\n\n");
printf ("USAGE: %s [options]\n", prgname);
printf ("Options:\n");
printf ("  -v   Verbose mode\n");
printf ("  -n   dev_id (from 'devices' table) \n ");
printf ("  -e   send data to epics (deactivated by default) \n");
printf ("  -h   send data to historic DB  (deactivated by default) \n");
printf ("  -m   send data to MEMORY DB  (deactivated by default) \n");
printf ("  -f   send data to FILES  (deactivated by default) \n");
printf ("  -a   send alarms  (deactivated by default) \n");
printf ("  -d   database server  (default 127.0.0.1) \n");
printf ("  -s   sample period (default 10 seconds) \n");
printf ("  -r   reconnect period (default 10 minutes) \n");
printf ("  -p   minimum priority to trigger voice messages (default=8) \n");
printf ("============================================================\n\n");
fflush(stdout);
}

//------------------------------------------------------------------------

void exitfunction()
{
kill(_child_pid, SIGTERM);	
//kill(0, SIGTERM);		// kill everyone on his group (all his children)
exit(0);
}

//------------------------------------------------------------------------

void fin (int signum)
{
kill(_child_pid, SIGTERM);		
//exitfunction();
exit(0);
}

//------------------------------------------------------------------------

// evaluate alarm conditions based on traffic (heuristic condition pending!!!)

int eval_alarm(device_data *d, int n)
{
struct tm   stm;

// return to normal conditions are always evaluated, to avoid long (and unrallistic) intervals of failure
if (  d->ifs[n].alarm_status != ALARM_OK )
	if (detect_normal_traffic_01(d, n))
		{
		d->ifs[n].alarm_status = ALARM_OK;
		report_alarm(d,n);
		}

localtime_r(&(d->ifs[n].last_access.time), &stm);

// EVALUATE FIRST EXCLUSION INTERVAL ONLY IF INI-FIN values have been fully specified
if (in_interval(&stm, d->ifs[n].exc_01_ini_h, d->ifs[n].exc_01_ini_m, d->ifs[n].exc_01_fin_h, d->ifs[n].exc_01_fin_m))
	return(1);

// second exclusion interval
if (in_interval(&stm, d->ifs[n].exc_02_ini_h, d->ifs[n].exc_02_ini_m, d->ifs[n].exc_02_fin_h, d->ifs[n].exc_02_fin_m))
	return(1);

if (d->ifs[n].alarm_lo < 30)		// zero threshold means no alarm for this interface!
  return(1);

// following calculation will be only evaluated if we are not (already) in ALARM status
if (  d->ifs[n].alarm_status != ALARM_ERROR )	
	{
	int			CALC_IN_THR=5.0;
	int			CALC_OUT_THR=5.0;
	
	double      incalc=0, outcalc=0;
	
	detect_low_traffic_01((d->ifs[n].ibw_buf), &incalc);
	detect_low_traffic_01((d->ifs[n].obw_buf), &outcalc);

	// INPUT condition:
	if ( incalc > CALC_IN_THR && d->ifs[n].ibw_buf[0] < d->ifs[n].alarm_lo && d->ifs[n].ibw_buf[1] < d->ifs[n].alarm_lo )
	    {
		d->ifs[n].alarm_status = ALARM_ERROR;
		report_alarm(d,n);
		}
	else if ( outcalc > CALC_OUT_THR && d->ifs[n].obw_buf[0] < d->ifs[n].alarm_lo && d->ifs[n].obw_buf[1] < d->ifs[n].alarm_lo )
	    {
		d->ifs[n].alarm_status = ALARM_ERROR;
		report_alarm(d,n);
		}
	}
return(1);
}	

//------------------------------------------------------------------------

int save_to_file( device_data *d )
{
FILE        *f=NULL;
char        fname[500];
struct tm   stm;
int 		i=0;
double      inavg=0, outavg=0;
double      incalc=0, outcalc=0;

if (_verbose > 3)
  printf("\n\n Saving data to file:");

for (i=0 ; i<d->ifs_n ; i++)
  {
  if ( strlen(d->ifs[i].file_var_name) > 0 )
	{
	sprintf(fname, "/usr/local/data/bw/%s", d->ifs[i].file_var_name);
	if  ( (f = fopen(fname, "a")) != NULL )
  	  {
	  localtime_r(&(d->ifs[i].last_access.time), &stm);

	  if (! avg_basic((d->ifs[i].ibw_buf), 0, (MAXAVGBUF-1), &inavg) )
		  inavg=0;

	  if (! avg_basic((d->ifs[i].obw_buf), 0, (MAXAVGBUF-1), &outavg) )
		  outavg=0;

	  detect_low_traffic_01((d->ifs[i].ibw_buf), &incalc);
	  detect_low_traffic_01((d->ifs[i].obw_buf), &outcalc);
	  
	  fprintf(f, "%li.%03i,%4i%02i%02i%02i%02i%02i.%03i,%lli,%lli,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%.2lf,%li,%li,%.4lf,%.4lf\r\n",
			  d->ifs[i].last_access.time, d->ifs[i].last_access.millitm, 
              stm.tm_year+1900, stm.tm_mon+1, stm.tm_mday, stm.tm_hour, stm.tm_min, stm.tm_sec, 
              d->ifs[i].last_access.millitm, 
	          d->ifs[i].ibytes, d->ifs[i].obytes, d->ifs[i].ibw/1000, d->ifs[i].obw/1000, 
	          d->ifs[i].ibw_a/1000, d->ifs[i].obw_a/1000,
	          d->ifs[i].ibw_b/1000, d->ifs[i].obw_b/1000, d->ifs[i].ibw_c/1000, d->ifs[i].obw_c/1000, 
			  inavg/1000, outavg/1000,
	          d->ifs[i].ierrors, d->ifs[i].oerrors,
			  incalc, outcalc
			  );
      fclose(f);
      }
    usleep(5000);
    }
  }
return(1);
}    

                          
//------------------------------------------------------------------------

// this is the child. he invokes telnet/ssh iteratively, in case
// parent decides to end communication

int   process_child( device_data *d )
{
char 	aux[500];
int     end=0, ret=0;

while (!end)
  {
  if (d->cli_acc == CLI_ACCESS_TELNET)
	{
    sprintf(aux, "/usr/bin/telnet -e ''"); 
    ret = system(aux);
	}
  else if (d->cli_acc == CLI_ACCESS_SSH)
	{
	if (d->getrunn == 20) 
		sprintf(aux, "/usr/bin/ssh ubnt@%s", d->ip);
	else
		sprintf(aux, "/usr/bin/ssh adc@%s", d->ip);

	ret = system(aux);
	}

  if (_verbose > 3)
	{
	printf( "\n\n SYSTEM CALL EXIT!!! \n\n");
	fflush(stdout);
	}

  usleep(300000);
  }	


ret = ret;
	             
return(1);
}

//------------------------------------------------------------------------

// this process send commands to his child, then receive and proceses answers
int   process_parent( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id )
{
int 				end=0;
int 				iface=0;
static time_t		old_time=0, old_db_time=0;
time_t				current_time=0;
int 				comm_error=0;

old_time = current_time = time(NULL);
if (d->authenticate && d->authenticate(d, infd, outfd))
  {
  if (_verbose > 2)	
	printf("\n Device hostname: |%s| \n",d->hostname); 

  while (!end)
    {
	  
    for (iface=0 ; iface<d->ifs_n ; iface++)
  	  {
	  if (d->ifs[iface].enable > 0 && !comm_error)
		{
		if (d->process)  
			d->process(d, infd, outfd, child_pid, dev_id, iface);
		}	
	  }	
	
	if (d->ncycle > 0 && !comm_error)     
	  {
	  if (_to_files)
    	save_to_file( d );

	  if (_to_memdb)
  		to_db_mem ( d );

	  if (_to_hisdb)
		to_db_hist ( d );
	  }	

    if (d->ncycle < 1000)  // for start condition
		d->ncycle++;

    // preventive disconnection, just in case  
    if ( ((current_time = time(NULL)) > (old_time + (_reconnect_period * 60))) || comm_error )
  	  {
  	  old_time = current_time;
	  dbread (d, dev_id);

	  if (_verbose > 2)			printf("\n It's high time for a reconnection!");
		
	  if (d->disconnect)
              d->disconnect( d, infd, outfd );
      sleep (1);
	  if (d->authenticate && d->authenticate(d, infd, outfd))
		comm_error=0;
	  else	
		{
		comm_error=1;
  		printf("\n Authentication error in reconnection! \n");
  		}

  	  sleep (9);
  	  }   
    else  
  	  sleep(_sample_period);  


    // preventive disconnection from DB
    if ( ((current_time = time(NULL)) > (old_db_time + 300)) || comm_error )
  	  {
  	  old_db_time = current_time;
	  db_disconnect();
	  db_connect();
	  }

	db_keepalive(_process_name);
  	}
  	
  if (d->disconnect)
    d->disconnect( d, infd, outfd );
  }

kill(child_pid, SIGINT);          
return(1);
}

//------------------------------------------------------------------------

int init_dev_pointers(device_data *d)
{

if (d->vendor_id == VENDOR_ID_RAISECOM)
    {
    d->authenticate = raisecom_authenticate;
    d->process = raisecom_process;
    d->parse_bw = raisecom_parse_bw;
    d->disconnect = raisecom_disconnect;
    }
else if (d->vendor_id == VENDOR_ID_CISCO)
    {
    d->authenticate = cisco_authenticate;
    d->process = cisco_process;
    d->parse_bw = cisco_parse_bw;
    d->disconnect = cisco_disconnect;
    }
else if (d->vendor_id == VENDOR_ID_DATACOM)
    {
    d->authenticate = dm_authenticate;
    d->process = dm_process;
    d->parse_bw = dm_parse_bw;
    d->disconnect = dm_disconnect;
    }
else if (d->vendor_id == VENDOR_ID_AT)
    {
    d->authenticate = at_authenticate;
    d->process = at_process;
    d->parse_bw = at_parse_bw;
    d->disconnect = at_disconnect;
    }
else if (d->vendor_id == VENDOR_ID_CERAGON)
    {
    d->authenticate = ceragon_authenticate;
    d->process = ceragon_process;
    d->parse_bw = ceragon_parse_bw;
    d->disconnect = ceragon_disconnect;
    }
else if (d->vendor_id == VENDOR_ID_UBNT)
    {
    d->authenticate = ubnt_authenticate;
    d->disconnect = ubnt_disconnect;

	if ( d->model_id == UBNT_EDGESWITCH )
	  {
	  d->authenticate = ubnt_authenticate_es;
	  d->process = ubnt_process_es;
  	  d->parse_bw = ubnt_parse_bw_es;
      }
	else if ( d->model_id == UBNT_TOUGHSWITCH )
	  {
	  d->process = ubnt_process_ts;
  	  d->parse_bw = ubnt_parse_bw_ts;
      }
	else if ( d->model_id == UBNT_AF25 )
	  {
	  d->process = ubnt_process_af25;
  	  d->parse_bw = ubnt_parse_bw_af25;
      }
	else if ( d->model_id == UBNT_AF5 )
	  {
	  d->process = ubnt_process_af5;
  	  d->parse_bw = ubnt_parse_bw_af5;
      }
	else if ( d->model_id == UBNT_MCA )
	  {
	  d->process = ubnt_process_mca;
  	  d->parse_bw = ubnt_parse_bw_mca;
      }
    else
  	  {  
  	  d->process = ubnt_process;
  	  d->parse_bw = ubnt_parse_bw;
  	  }
    }
else if (d->vendor_id == VENDOR_ID_SERVER)
    {
    d->authenticate = server_authenticate;
    d->process = server_process;
    d->parse_bw = server_parse_bw;
    d->disconnect = server_disconnect;
    }
else if (d->vendor_id == VENDOR_ID_SAF)
    {
    d->authenticate = saf_authenticate;
    d->disconnect = saf_disconnect;

	if ( d->model_id == SAF_CFIP_PHOENIX )
	  {
	  d->process = saf_process_phoenix;
  	  d->parse_bw = saf_parse_bw_phoenix;
      }
	}
else if (d->vendor_id == VENDOR_ID_JUNIPER)
    {
    d->authenticate = junos_authenticate;
    d->disconnect = junos_disconnect;

	if ( d->model_id == JUNIPER_EX4200 )
	  {
	  d->process = junos_process;
  	  d->parse_bw = junos_parse_bw;
      }
	}


return(1);
}

//------------------------------------------------------------------------

int test_conditions(device_data *d)
{
	
if (d->vendor_id < VENDOR_ID_CISCO || d->vendor_id > VENDOR_ID_RAISECOM)
	{
	printf("\n\n Error!   Unknown vendor (%i) for device %i %s\n\n", d->vendor_id, d->dev_id, d->name);
	return(0);
	}

return(1);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------

int main(int argc, char *argv[])
{
int     		opt=0;
int 			dev_id=-1;
int 			retfd=0;

device_data		devd;

strcpy(server, "127.0.0.1");

while ((opt = getopt(argc, argv, "s:d:n:r:ehfmva")) != -1)
    {
    switch (opt)
        {
        case 'v':
            _verbose++;
        break;

        case 'e':
            _to_epics=1;
        break;

        case 'f':
            _to_files=1;
        break;

        case 'm':
            _to_memdb=1;
        break;

        case 'h':
            _to_hisdb=1;
        break;

        case 'a':
            _send_alarm=1;
        break;

        case 'n':
      		if(optarg)
          	dev_id=atoi(optarg);
        break;

        case 'p':
      		if(optarg)
          	_speech_prio=atoi(optarg);
        break;

        case 's':
      		if(optarg)
				_sample_period = (atoi(optarg) >= 1) ? atoi(optarg) : 1;
        break;

        case 'r':
      		if(optarg)
				_reconnect_period = (atoi(optarg) >= 5) ? atoi(optarg) : 1;
        break;

        case 'd':
	    if(optarg)
        	strcpy(server, optarg);
        break;


        default: /* ’?’ */
            printUsage(argv[0]);
            exit(EXIT_FAILURE);
        }
    }

printf("\n Program %s started. PID: %i.  ", argv[0], getpid()); fflush(stdout);
printf("\n Compiled: %s...\n\n", __TIMESTAMP__); fflush(stdout);

if (dev_id<0)
  {
  printUsage(argv[0]);
  exit(EXIT_FAILURE);
  }

if ( _to_epics==0 && _to_files==0 && _to_memdb==0 && _to_hisdb==0 )
  {
  printf("\n\n ATENTION:   collected data will not be recorded to DB, MEM, FILE, etc!  (It's your call dude (your time, your CPU!) \n\n");
//  exit(EXIT_FAILURE);
  }

sprintf(_process_name, "TRAFSTATS_%i", dev_id);


memset(&devd, 0, sizeof(device_data));
while (dbread (&devd, dev_id) <= 0)
  {
  printf("\n\n No interfaces configured on 'devices_bw' for this device ID (%i) ! \n\n", dev_id);
  sleep(1);
  };

if (test_conditions(&devd))
	{	
	delete_from_db_mem (dev_id);

	// initialization process according to detect device type!
	init_dev_pointers(&devd);

	// we fork to call telnet / ssh
	_child_pid = forkpty(&retfd, NULL, NULL, NULL);
	if( _child_pid < 0)
	  {
	  printf("\n\n FORK ERROR !!! \n\n");
	  exit(-1);
	  }
	else if (_child_pid == 0)		// we are in the child
	  {
	  process_child(&devd);
	  }
	else 
	  {
	  signal(SIGTERM, fin);
	  signal(SIGQUIT, fin);
	  signal(SIGINT, fin);

	  atexit(exitfunction);
	  process_parent(&devd, retfd, retfd, _child_pid, dev_id);
	  }
	}

exit(0);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------

