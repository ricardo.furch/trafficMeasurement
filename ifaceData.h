
#define			MAXBUF				200
#define			MAXAVGBUF			10

#define			TELNET_ACCESS		1
#define			SSH_ACCESS			2

// CLI_ACC   access types on CLI
#define			CLI_ACCESS_TELNET		1
#define			CLI_ACCESS_SSH			2

// VENDOR_ID	types 
#define			VENDOR_ID_CISCO			1
#define			VENDOR_ID_AT			2
#define			VENDOR_ID_UBNT			3
#define			VENDOR_ID_CERAGON		4
#define			VENDOR_ID_RAD			5
#define			VENDOR_ID_SERVER		6
#define			VENDOR_ID_DATACOM		7
#define			VENDOR_ID_SAF			8
#define			VENDOR_ID_JUNIPER		9
#define			VENDOR_ID_RAISECOM		10

// MODEL_ID	types 
#define			CISCO_2921			1
#define			CISCO_2950			2
#define			CISCO_2960			3
#define			CISCO_3550			4
#define			CISCO_3560			5
#define			CISCO_3600			6
#define			CISCO_3925			7
#define			CISCO_6500			8
#define			CISCO_7200			9
#define			CISCO_7600			10
#define			CISCO_AIR1310		11
#define			CISCO_ASA5520		12

#define			AT8000				1
#define			AT8088				2
#define			AT8012				3
#define			AT9408				4

#define			UBNT_NBM5			1
#define			UBNT_NSM5			2
#define			UBNT_RKM5			3
#define			UBNT_UAPINDOOR		4
#define			UBNT_UAPOUTDOOR		5
#define			UBNT_TOUGHSWITCH	6
#define			UBNT_AF25			10
#define			UBNT_AF5			11
#define			UBNT_MCA			12
#define			UBNT_R5AC			13
#define			UBNT_EDGESWITCH		14

#define			CERAGON_IP10E		1

#define			RAD_AIRMUX			1

#define			SERVER_DEBIAN		1

#define			DATACOM_2104		1

#define			SAF_CFIP_PHOENIX	1

#define			JUNIPER_EX4200		1	

#define			RAISECOM_RAX711		1	


// devices table:  'getrunn' field is used to distinguish device brand/model by:

#define			CISCO_TELNET_SUR		1				
#define			CISCO_TELNET_NORTE		2	
#define			CISCO_SSH_NORTE			3				
#define			CISCO_TELNET_CORDOBA	4				
#define			AT8000_NORTE			5				
#define			AT8000_SUR				6				
#define			CISCO_FW				7
#define			CISCO_TACACS			8
#define			AT8000_TACACS			9
#define			CISCO_TACACS_SUR		10
#define			CISCO_SSH_TACACS		11	
#define			UBNT_SSH				12
#define			UBNT_UNIFI_SSH			13
#define			JUNIPER_SSH				14
#define			CERAGON_TELNET			15


#define			ALARM_ERROR			100
#define			ALARM_OK			101

// structure to store interfaces properties
typedef struct iface_data
  {
  int 				ifid;
  unsigned char			enable;
  unsigned char			status;					// 0: uninitialized, 1: running					
  char				name[MAXBUF];
  char				description[MAXBUF];
  char				peername[MAXBUF];
  struct timeb			last_access;
  long long int			msec, msec_prev;
  long long int			ibytes, ibytes_prev, ibytes_prev_prev ;
  long long int			obytes, obytes_prev, obytes_prev_prev ;
  long int			oerrors, ierrors;
  double			ibw, obw;				// input and output bandwidth (bps)
  double			ibw_a, obw_a;		    // bw measure with 'moving average' ej:  0.2*current + 0.8*previous	
  double			ibw_b, obw_b;
  double			ibw_c, obw_c;
  long int			ibw_buf[MAXAVGBUF], obw_buf[MAXAVGBUF]; 
  char				file_var_name[MAXBUF];
  unsigned char		 	to_epics;
  int				alarm_lo;				// alarm threshold (in kbps), minimum acceptable value!
  int				prio_lo;				// alarm priority for LOW traffic level
  int				alarm_status;
  short int			exc_01_ini_h, exc_01_ini_m, exc_01_fin_h, exc_01_fin_m ;	
  short int			exc_02_ini_h, exc_02_ini_m, exc_02_fin_h, exc_02_fin_m ;	
  }iface_data;

// structure to store device properties

typedef struct device_data
  {
  int 			dev_id;
  unsigned char 	enable;
  unsigned char 	status;					// 0: uninitialized, 1: running					
  char 			access_type;			// 0: none, 1: telnet, 2:ssh
  char 			name[MAXBUF];
  char 			ip[MAXBUF];
  char 			hostname[MAXBUF];
  char 			ena_prompt[MAXBUF];
  char 			dis_prompt[MAXBUF];
  struct timeb		last_access;
  int 			ifs_n;
  iface_data		*ifs;	
  long long int 	ncycle;
  short int 		getrunn;
  short	int		cli_acc;
  short	int		vendor_id;
  short	int		model_id;

  
  int (*init)( struct device_data *d);
  int (*authenticate)( struct device_data *d, int infd, int outfd );
  int (*process)( struct device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
  int (*parse_bw)( struct device_data *d, int iface, char *b);
  int (*disconnect)( struct device_data *d, int infd, int outfd );

  }device_data;

// structure to handle a single  list of devices
typedef struct device_list
  {
  int 				n;
  device_data       *d;
  }device_list;

typedef struct ep_pv
    {
    int         id;
    char        name[55];
    double      old_value, value;
    time_t      db_last_update;
    time_t      sample_time;
//    chid        chanid;
    char        has_chid;
    char        epics_error;
    }ep_pv;

typedef struct pv_list
    {
    time_t              ltime;
    int                 pid;
    int                 n;
    ep_pv              *data;
    }pv_list;

extern int _verbose;

// util.c
char *adc_ltrim(char *s);
char *adc_rtrim(char *s);
char *adc_trim(char *s);
int str_firstchar(char *s);
int str_extract(char *s, int n1, int n2, char *ret);
int get_hostname(char *buffer_aux, char *hostname_str);
int read_stream(int fd, char *ptr, int maxbytes);
int wait_for_string(int fd, long int tout, int retries, char *expect, char *buffer, int maxbytes, int delaytime);
int wait_for_string_nonzero(int fd, long int tout, int retries, char *expect, char *buffer, int maxbytes, int delaytime);
int read_wait(int fd, long int tout, int retries, char *buffer, int maxbytes);
int str_normalize(unsigned char *str, int slen);
int send_cmd(int fd, char  *cmd);
int avg_basic(long int * buffer, short ini, short fin, double *avg);
int detect_low_traffic_01(long int * buffer, double *calc);
int detect_normal_traffic_01(device_data *d, int n);
int in_interval(struct tm *current, short inih, short inim, short finh, short finm);
int str_extract_from(char *s, int c1, char *ret);
int send_control_d(int fd);

// db.c
int db_connect();
int db_disconnect();
int to_db_mem (device_data *d);
int dbread (device_data *devd, int devid);
int to_db_hist (device_data *d);
int delete_from_db_mem (int devid);
int db_keepalive(char *name);
int report_alarm(device_data *d, int n);

char *adc_ltrim(char *s);
char *adc_rtrim(char *s);
char *adc_trim(char *s);

//cisco.c
int cisco_authenticate( device_data *d, int infd, int outfd );
int cisco_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int cisco_parse_bw( device_data *d, int iface, char *b);
int cisco_disconnect( device_data *d, int infd, int outfd );

//at8000.c
int at_authenticate( device_data *d, int infd, int outfd );
int at_parse_bw( device_data *d, int n, char *b);
int at_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int at_disconnect( device_data *d, int infd, int outfd );

//ceragon.c
int ceragon_get_counter(char *str, char *smatch, long long *val);
int ceragon_authenticate( device_data *d, int infd, int outfd );
int ceragon_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int ceragon_parse_bw( device_data *d, int iface, char *b);
int ceragon_disconnect( device_data *d, int infd, int outfd );

//ubnt.c
int ubnt_authenticate( device_data *d, int infd, int outfd );
int ubnt_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int ubnt_parse_bw( device_data *d, int iface, char *b);
int ubnt_disconnect( device_data *d, int infd, int outfd );
int ubnt_process_ts( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int ubnt_parse_bw_ts( device_data *d, int iface, char *b);
int ubnt_process_af25( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int ubnt_parse_bw_af25( device_data *d, int iface, char *b);
int ubnt_process_af5( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int ubnt_parse_bw_af5( device_data *d, int iface, char *b);
int ubnt_process_mca( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int ubnt_parse_bw_mca( device_data *d, int iface, char *b);
int ubnt_authenticate_es( device_data *d, int infd, int outfd );
int ubnt_process_es( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int ubnt_parse_bw_es( device_data *d, int iface, char *b);

//server.c
int server_authenticate( device_data *d, int infd, int outfd );
int server_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int server_parse_bw( device_data *d, int iface, char *b);
int server_disconnect( device_data *d, int infd, int outfd );

// datacom.c
int dm_authenticate( device_data *d, int infd, int outfd );
int dm_parse_bw( device_data *d, int n, char *b);
int dm_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int dm_disconnect( device_data *d, int infd, int outfd );

//saf.c  SAF Phoenix
int saf_authenticate( device_data *d, int infd, int outfd );
int saf_parse_bw_phoenix( device_data *d, int n, char *b);
int saf_process_phoenix( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int saf_disconnect( device_data *d, int infd, int outfd );

//juniper.c
int junos_authenticate( device_data *d, int infd, int outfd );
int junos_parse_bw( device_data *d, int n, char *b);
int junos_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int junos_disconnect( device_data *d, int infd, int outfd );

//raisecom.c
int raisecom_authenticate( device_data *d, int infd, int outfd );
int raisecom_parse_bw( device_data *d, int n, char *b);
int raisecom_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface );
int raisecom_disconnect( device_data *d, int infd, int outfd );

// main.c
int eval_alarm(device_data *d, int n);


//------------------------------------------------------------------------
//------------------------------------------------------------------------
