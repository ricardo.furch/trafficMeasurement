/* 
  Author: rfurch
 
  TODO:
*/


#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <time.h>
#include <pty.h>
#include <sys/time.h>
#include <sys/select.h>
#include <stdio.h>
#include <ctype.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <semaphore.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <signal.h>
#include <pty.h>
#include <math.h>

#include <sys/timeb.h>

#include "ifaceData.h"                                                                                                     

extern int             _verbose;
extern pid_t           _child_pid;
extern pv_list         _pvl;
extern int             _to_epics;
extern int             _to_files;
extern int             _to_memdb;
extern int             _to_hisdb;
extern int             _send_alarm;
extern char            _process_name[];

extern char     		server[];


//------------------------------------------------------------------------

// parses lines like:
// KOutGoodOctets           104928225883
// looking for a matchig experession (KOutGoodOctets)
// an retrieves value (AS LONG LONG)

int ceragon_get_counter(char *str, char *smatch, long long *val)
{
char *x=NULL, *y=NULL;

if (!str || !smatch || !val)
  return(0);
  
if ( !(x=strstr(str, smatch) ) )		// substring not found
  return(0);
  
x+=strlen(smatch);
while (*x==' ' || *x==9)		// space or TAB
  x++;

y=x;
while (  (*y>='0' && *y<='9') || *y=='.'  || *y==','  )		// space or TAB
  y++;
*y=0;  

*val=atoll(x);
return(1);
}


//------------------------------------------------------------------------

int 	ceragon_authenticate( device_data *d, int infd, int outfd )
{
char		buffer_aux[3000];
char		straux[300];

if (! (d->getrunn == CERAGON_TELNET))
	return(0);

sprintf(straux, "open  %s\n", d->ip);
send_cmd(outfd, straux);
if ( ! wait_for_string(infd, 3000, 3, "ogin:", buffer_aux, 2000, 300 ) )
	{
	printf("\n\n Authentication error 1 \n\n ");
	fflush(stdout);
	return(0);
	}

send_cmd(outfd, "adc\n");
if ( ! wait_for_string(infd, 5000, 3, "word", buffer_aux, 2000, 300) )
	{
	printf("\n\n Authentication error 2 \n\n ");
	fflush(stdout);
	return(0);
	}

send_cmd(outfd, "nUevo1983\n");
if ( ! wait_for_string(infd, 5000, 3, ">", buffer_aux, 2000, 300) )
	{
	printf("\n\n Authentication error 3 \n\n ");
	fflush(stdout);
	return(0);
	}

send_cmd(outfd, "\n");
if ( ! wait_for_string(infd, 5000, 3, ">", buffer_aux, 2000, 300) )
  {
  printf("\n\n Authentication error 3 \n\n ");
  fflush(stdout);
  return(0);
  }

get_hostname(buffer_aux, d->hostname);
 
return(1);
}

//------------------------------------------------------------------------

int ceragon_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface )
{
char 				commandstr[300];
char 				buffer_aux[8000];
int 				comm_error=0;

sprintf(commandstr, "cd /diagnostics/rmon \n");
send_cmd(outfd, commandstr);

sprintf(commandstr, "get-statistics %s\n", d->ifs[iface].name);
send_cmd(outfd, commandstr);
sprintf(commandstr, "%c", 32);
send_cmd(outfd, commandstr);
sprintf(commandstr, "q");
send_cmd(outfd, commandstr);

if (_verbose > 1)
	{
	printf("\n wait for prompt: |%s| \n ", d->hostname);
	fflush(stdout);
	}

// BW calculations !!!
if ( wait_for_string(infd, 5000, 3, d->hostname, buffer_aux, 2000, 300) )
	{
	if (d->parse_bw)
		d->parse_bw( d, iface, buffer_aux );
	comm_error=0;
	}
else
	comm_error=1;

comm_error=comm_error;

return(1);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// this routine parses 'show int counters (Allied Telesis)' command, 
// parsing a line like .....
// d: device data structure
// n: index of interface to parse
// b: string to parse ('show int' dump)

int ceragon_parse_bw( device_data *d, int iface, char *b)
{
char 					s2[10] = "\n";
char 					*ptr=NULL;
long long int   		lli1=0, lli2=0;
struct timeb 			stb;
double					delta_t=0;


ftime(&stb); 
d->ifs[iface].last_access=stb;
d->ifs[iface].msec_prev = d->ifs[iface].msec;
d->ifs[iface].msec = stb.time*1000+stb.millitm;
delta_t =  ((double)(d->ifs[iface].msec - d->ifs[iface].msec_prev))/1000; // delta t in seconds 

d->ifs[iface].ibytes_prev_prev = d->ifs[iface].ibytes_prev;
d->ifs[iface].obytes_prev_prev = d->ifs[iface].obytes_prev;
d->ifs[iface].ibytes_prev = d->ifs[iface].ibytes;
d->ifs[iface].obytes_prev = d->ifs[iface].obytes;

// we set this values to 0 to take some action below
d->ifs[iface].ibytes = 0;
d->ifs[iface].obytes = 0;

ptr = strtok( b, s2 );    // Primera llamada => Primer token
while( (ptr = strtok( NULL, s2 )) != NULL )    // Posteriores llamadas
	{
	if (strstr(ptr, "InGoodOctets")) 
		{
		if ( ceragon_get_counter(ptr, "InGoodOctets", &lli1) )
  	  	  d->ifs[iface].ibytes = lli1;
		}
	else if (strstr(ptr, "OutGoodOctets")) 
		{
		if (ceragon_get_counter(ptr, "OutGoodOctets", &lli2))
  		  d->ifs[iface].obytes = lli2;
		}
	}

if ( d->ifs[iface].ibytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].ibytes_prev > d->ifs[iface].ibytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxin=0;
  
  auxin = (8*(double)(d->ifs[iface].ibytes - d->ifs[iface].ibytes_prev)) / delta_t;
  if (auxin < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].ibw =  auxin;
  
  if ( d->ifs[iface].ibytes_prev_prev == 0 ) // second pass after starting the program, it's a good idea to use current 'instant' traffic as average!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].ibw_buf[j] = d->ifs[iface].ibw;
	d->ifs[iface].ibw_a	= d->ifs[iface].ibw_b = d->ifs[iface].ibw_c = d->ifs[iface].ibw;
	}
  else
  	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].ibw_buf[1]), &(d->ifs[iface].ibw_buf[0]), sizeof((d->ifs[iface].ibw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].ibw_buf[0] = d->ifs[iface].ibw;
	  
	  d->ifs[iface].ibw_a =   0.5 * d->ifs[iface].ibw + 0.5 * d->ifs[iface].ibw_a;
	  d->ifs[iface].ibw_b =   0.1 * d->ifs[iface].ibw + 0.9 * d->ifs[iface].ibw_b;
	  d->ifs[iface].ibw_c =   0.02 * d->ifs[iface].ibw + 0.98 * d->ifs[iface].ibw_c;
	  }
  }

if ( d->ifs[iface].obytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].obytes_prev > d->ifs[iface].obytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxout=0;
  
  auxout = (8*(double)(d->ifs[iface].obytes - d->ifs[iface].obytes_prev)) / delta_t;
  if (auxout < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].obw =  auxout;

  if ( d->ifs[iface].obytes_prev_prev == 0 ) // idem!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].obw_buf[j] = d->ifs[iface].obw;
	d->ifs[iface].obw_a	= d->ifs[iface].obw_b = d->ifs[iface].obw_c = d->ifs[iface].obw;
  }
else
	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].obw_buf[1]), &(d->ifs[iface].obw_buf[0]), sizeof((d->ifs[iface].obw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].obw_buf[0] = d->ifs[iface].obw;

	  d->ifs[iface].obw_a =   0.5 * d->ifs[iface].obw + 0.5 * d->ifs[iface].obw_a;
	  d->ifs[iface].obw_b =   0.1 * d->ifs[iface].obw + 0.9 * d->ifs[iface].obw_b;
	  d->ifs[iface].obw_c =   0.02 * d->ifs[iface].obw + 0.98 * d->ifs[iface].obw_c;
	  }
  }

if (d->ncycle > 10)  // alarms only after startup window
  if (_send_alarm)
	eval_alarm(d, iface);

  
if (_verbose > 1)
  {
  printf("\n\n --------------------------- ");
  printf("\n interface: %s  (%s)", d->ifs[iface].name, d->ifs[iface].description);
  printf("\n delta t:  %lf", delta_t);
  printf("\n ibw: %lf ibw_a: %lf", d->ifs[iface].ibw, d->ifs[iface].ibw_a);
  printf("\n obw: %lf obw_a: %lf", d->ifs[iface].obw, d->ifs[iface].obw_a);
  printf("\n ibytes: %lli obytes: %lli", d->ifs[iface].ibytes, d->ifs[iface].obytes);
  printf("\n ibytes prev: %lli obytes prev: %lli", d->ifs[iface].ibytes_prev, d->ifs[iface].obytes_prev);
  printf("\n --------------------------- \n\n"); 
  fflush(stdout);
  }  

return(0);
}

//------------------------------------------------------------------------------------------

int 	ceragon_disconnect( device_data *d, int infd, int outfd )
{
send_cmd(outfd, "logout\n");

if (_verbose > 4)
  printf("\n Disconnection request SENT (logout) \n");
  
fflush(stdout);  
return(1);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
