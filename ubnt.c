/* 
  Author: rfurch
  TODO:
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <time.h>
#include <pty.h>
#include <sys/time.h>
#include <sys/select.h>
#include <stdio.h>
#include <ctype.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <semaphore.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <signal.h>
#include <pty.h>
#include <math.h>

#include <sys/timeb.h>

#include "ifaceData.h"                                                                                                     

extern int             _verbose;
extern pid_t           _child_pid;
extern pv_list         _pvl;
extern int             _to_epics;
extern int             _to_files;
extern int             _to_memdb;
extern int             _to_hisdb;
extern int             _send_alarm;
extern char            _process_name[];

extern char     		server[];

//------------------------------------------------------------------------
//------------------------------------------------------------------------



int ubnt_authenticate_es( device_data *d, int infd, int outfd )
{
char		buffer_aux[3000];
char 		datap[300];

strcpy(datap,"nUevo1983\n");

// if access type is ssh, we might need to send 'yes' to add site to the known hosts list
if (  wait_for_string(infd, 3000, 3, "(yes/no)", buffer_aux, 2000, 300 ) )
	send_cmd(outfd, "yes\n");

if (strstr(buffer_aux, "sword:"))	
	{
	send_cmd(outfd, datap);
	if ( ! wait_for_string(infd, 5000, 3, ">", buffer_aux, 2000, 300) )
		{
		printf("\n\n Authentication error ub1 \n\n ");
		fflush(stdout);
		return(0);
		}
	}
else
	{
	if ( ! wait_for_string(infd, 5000, 3, "word", buffer_aux, 2000, 300) )
		{
		printf("\n\n Authentication error ub2 \n\n ");
		fflush(stdout);
		return(0);
		}

	send_cmd(outfd, datap);
	if ( ! wait_for_string(infd, 5000, 3, ">", buffer_aux, 2000, 300) )
		{
		printf("\n\n Authentication error ub3 \n\n ");
		fflush(stdout);
		return(0);
		}
	}	  

send_cmd(outfd, "\n");
if ( ! wait_for_string(infd, 5000, 3, ">", buffer_aux, 2000, 300) )
  {
  printf("\n\n Authentication error ub4 \n\n ");
  fflush(stdout);
  return(0);
  }

send_cmd(outfd, "enable\n");


if ( ! wait_for_string(infd, 5000, 3, "word", buffer_aux, 2000, 300) )
	{
	printf("\n\n Authentication error ub5 \n\n ");
	fflush(stdout);
	return(0);
	}
send_cmd(outfd, datap);
if ( ! wait_for_string(infd, 5000, 3, "#", buffer_aux, 2000, 300) )
	{
	printf("\n\n Authentication error ub6 \n\n ");
	fflush(stdout);
	return(0);
	}

send_cmd(outfd, "term len 0\n");
if ( ! wait_for_string(infd, 5000, 3, "#", buffer_aux, 2000, 300) )
  {
  printf("\n\n Authentication error ub7 \n\n ");
  fflush(stdout);
  return(0);
  }

send_cmd(outfd, "\n");
if ( ! wait_for_string(infd, 5000, 3, "#", buffer_aux, 2000, 300) )
  {
  printf("\n\n Authentication error ub7 \n\n ");
  fflush(stdout);
  return(0);
  }

get_hostname(buffer_aux, d->hostname);

strcpy(d->ena_prompt, d->hostname);
strcpy(d->dis_prompt, d->hostname);
strcat(d->ena_prompt, "#");
strcat(d->dis_prompt, ">");

return(1);
}


//------------------------------------------------------------------------

int ubnt_authenticate( device_data *d, int infd, int outfd )
{
char		buffer_aux[3000];
char 		datap[300];

if (d->getrunn == 20) 
	strcpy(datap,"comunica\n");
else
	strcpy(datap,"nUevoadc\n");

// if access type is ssh, we might need to send 'yes' to add site to the known hosts list
if (  wait_for_string(infd, 3000, 3, "(yes/no)", buffer_aux, 2000, 300 ) )
	send_cmd(outfd, "yes\n");

if (strstr(buffer_aux, "sword:"))	
	{
	send_cmd(outfd, datap);
	if ( ! wait_for_string(infd, 5000, 3, "#", buffer_aux, 2000, 300) )
		{
		printf("\n\n Authentication error ub1 \n\n ");
		fflush(stdout);
		return(0);
		}
	}
else
	{
	if ( ! wait_for_string(infd, 5000, 3, "word", buffer_aux, 2000, 300) )
		{
		printf("\n\n Authentication error ub2 \n\n ");
		fflush(stdout);
		return(0);
		}

	send_cmd(outfd, datap);
	if ( ! wait_for_string(infd, 5000, 3, "#", buffer_aux, 2000, 300) )
		{
		printf("\n\n Authentication error ub3 \n\n ");
		fflush(stdout);
		return(0);
		}
	}	  

send_cmd(outfd, "\n");
if ( ! wait_for_string(infd, 5000, 3, "#", buffer_aux, 2000, 300) )
  {
  printf("\n\n Authentication error ub4 \n\n ");
  fflush(stdout);
  return(0);
  }

get_hostname(buffer_aux, d->hostname);

return(1);
}

//------------------------------------------------------------------------

//  for airfiber
//  af get TxOctetsOK
//  af get RxOctetsOK

int ubnt_process_af25( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface )
{
	
char 				commandstr[300];
char 				buffer_aux[8000];
int 				comm_error=0;

sprintf(commandstr, "/usr/bin/af get RxOctetsOK ;  /usr/bin/af get TxOctetsOK \n");
send_cmd(outfd, commandstr);

if (_verbose > 1)
	{
	printf("\n wait for prompt: |%s| \n ", d->hostname);
	fflush(stdout);
	}
                        
// BW calculations !!!
if ( wait_for_string(infd, 5000, 3, d->hostname, buffer_aux, 2000, 300) )
	{
	if (d->parse_bw)
		d->parse_bw( d, iface, buffer_aux );
	comm_error=0;
	}
else
	comm_error=1;

comm_error=comm_error;
	
return(1);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// this routine parsess UBNT  commands 
// d: device data structure
// n: index of interface to parse
// b: string to parse ('show int' dump)
// toughswitch dums info via command:  tswconf mib statistics get PORT
// datadump: 
// RxGoodByte  <0x257b3bae>  RxGoodByte1 <0x00000000>  RxBadByte   <0x00000000>  
// RxBadByte1  <0x00000000>  RxOverFlow  <0x00000000>  Filtered    <0x0001e6a9>  
// TxBroad     <0x00009245>  TxPause     <0x00000000>  TxMulti     <0x0006127c>  
// TxByte      <0x287b842c>  TxByte1     <0x00000001>  TxCollision <0x00000000>  

int ubnt_parse_bw_af25( device_data *d, int iface, char *b)
{

char 					s2[10] = "\r\n";
char 					*ptr=NULL, *p=NULL;
struct timeb 			stb;
double					delta_t=0;

ftime(&stb); 
d->ifs[iface].last_access=stb;
d->ifs[iface].msec_prev = d->ifs[iface].msec;
d->ifs[iface].msec = stb.time*1000+stb.millitm;
delta_t =  ((double)(d->ifs[iface].msec - d->ifs[iface].msec_prev))/1000; // delta t in seconds 

d->ifs[iface].ibytes_prev_prev = d->ifs[iface].ibytes_prev;
d->ifs[iface].obytes_prev_prev = d->ifs[iface].obytes_prev;
d->ifs[iface].ibytes_prev = d->ifs[iface].ibytes;
d->ifs[iface].obytes_prev = d->ifs[iface].obytes;

// we set this values to 0 to take some action below
d->ifs[iface].ibytes = 0;
d->ifs[iface].obytes = 0;

ptr = strtok( b, s2 );    // first
	{
	if ( (p=strstr(ptr, "RxOctetsOK")) !=  NULL ) 
		{
		if( (ptr = strtok( NULL, s2 )) != NULL )    // second
			d->ifs[iface].ibytes = atoll(ptr);
		if( (ptr = strtok( NULL, s2 )) != NULL )    // second
			d->ifs[iface].obytes = atoll(ptr);
		}
	}

if ( d->ifs[iface].ibytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].ibytes_prev > d->ifs[iface].ibytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxin=0;
  
  auxin = (8*(double)(d->ifs[iface].ibytes - d->ifs[iface].ibytes_prev)) / delta_t;
  if (auxin < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].ibw =  auxin;
  
  if ( d->ifs[iface].ibytes_prev_prev == 0 ) // second pass after starting the program, it's a good idea to use current 'instant' traffic as average!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].ibw_buf[j] = d->ifs[iface].ibw;
	d->ifs[iface].ibw_a	= d->ifs[iface].ibw_b = d->ifs[iface].ibw_c = d->ifs[iface].ibw;
	}
  else
  	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].ibw_buf[1]), &(d->ifs[iface].ibw_buf[0]), sizeof((d->ifs[iface].ibw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].ibw_buf[0] = d->ifs[iface].ibw;
	  
	  d->ifs[iface].ibw_a =   0.5 * d->ifs[iface].ibw + 0.5 * d->ifs[iface].ibw_a;
	  d->ifs[iface].ibw_b =   0.1 * d->ifs[iface].ibw + 0.9 * d->ifs[iface].ibw_b;
	  d->ifs[iface].ibw_c =   0.02 * d->ifs[iface].ibw + 0.98 * d->ifs[iface].ibw_c;
	  }
  }

if ( d->ifs[iface].obytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].obytes_prev > d->ifs[iface].obytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxout=0;
  
  auxout = (8*(double)(d->ifs[iface].obytes - d->ifs[iface].obytes_prev)) / delta_t;
  if (auxout < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].obw =  auxout;

  if ( d->ifs[iface].obytes_prev_prev == 0 ) // idem!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].obw_buf[j] = d->ifs[iface].obw;
	d->ifs[iface].obw_a	= d->ifs[iface].obw_b = d->ifs[iface].obw_c = d->ifs[iface].obw;
  }
else
	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].obw_buf[1]), &(d->ifs[iface].obw_buf[0]), sizeof((d->ifs[iface].obw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].obw_buf[0] = d->ifs[iface].obw;

	  d->ifs[iface].obw_a =   0.5 * d->ifs[iface].obw + 0.5 * d->ifs[iface].obw_a;
	  d->ifs[iface].obw_b =   0.1 * d->ifs[iface].obw + 0.9 * d->ifs[iface].obw_b;
	  d->ifs[iface].obw_c =   0.02 * d->ifs[iface].obw + 0.98 * d->ifs[iface].obw_c;
	  }
  }

if (d->ncycle > 10)  // alarms only after startup window
  if (_send_alarm)
	eval_alarm(d, iface);

  
if (_verbose > 1)
  {
  printf("\n\n --------------------------- ");
  printf("\n interface: %s  (%s)", d->ifs[iface].name, d->ifs[iface].description);
  printf("\n delta t:  %lf", delta_t);
  printf("\n ibw: %lf ibw_a: %lf", d->ifs[iface].ibw, d->ifs[iface].ibw_a);
  printf("\n obw: %lf obw_a: %lf", d->ifs[iface].obw, d->ifs[iface].obw_a);
  printf("\n ibytes: %lli obytes: %lli", d->ifs[iface].ibytes, d->ifs[iface].obytes);
  printf("\n ibytes prev: %lli obytes prev: %lli", d->ifs[iface].ibytes_prev, d->ifs[iface].obytes_prev);
  printf("\n --------------------------- \n\n"); 
  fflush(stdout);
  }  

return(0);
}

//------------------------------------------------------------------------------------------

//  for airfiber 5 GHZ
//  af get TxOctetsOK
//  af get RxOctetsOK


int ubnt_process_af5( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface )
{
	
char 				commandstr[300];
char 				buffer_aux[8000];
int 				comm_error=0;

//  RFurch:  force PS1 to avoid problems with 'long commands'
sprintf(commandstr, "export PS1='AF5GHZ\x23' \n");
send_cmd(outfd, commandstr);
strcpy(d->hostname, "AF5GHZ#");

if ( wait_for_string(infd, 5000, 3, d->hostname, buffer_aux, 2000, 300) )
	{
	}


sprintf(commandstr, "/usr/bin/af get RxOctetsOK ;  /usr/bin/af get TxOctetsOK \n");
send_cmd(outfd, commandstr);

if (_verbose > 1)
	{
	printf("\n wait for prompt: |%s| \n ", d->hostname);
	fflush(stdout);
	}
                        
// BW calculations !!!
if ( wait_for_string(infd, 5000, 3, d->hostname, buffer_aux, 2000, 300) )
	{
	if (d->parse_bw)
		d->parse_bw( d, iface, buffer_aux );
	comm_error=0;
	}
else
	comm_error=1;

comm_error=comm_error;
	
return(1);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// this routine parsess UBNT  commands 
// d: device data structure
// n: index of interface to parse
// b: string to parse ('show int' dump)
// toughswitch dums info via command:  tswconf mib statistics get PORT
// datadump: 
// RxGoodByte  <0x257b3bae>  RxGoodByte1 <0x00000000>  RxBadByte   <0x00000000>  
// RxBadByte1  <0x00000000>  RxOverFlow  <0x00000000>  Filtered    <0x0001e6a9>  
// TxBroad     <0x00009245>  TxPause     <0x00000000>  TxMulti     <0x0006127c>  
// TxByte      <0x287b842c>  TxByte1     <0x00000001>  TxCollision <0x00000000>  

int ubnt_parse_bw_af5( device_data *d, int iface, char *b)
{

char 					s2[10] = "\r\n";
char 					*ptr=NULL, *p=NULL;
struct timeb 			stb;
double					delta_t=0;

ftime(&stb); 
d->ifs[iface].last_access=stb;
d->ifs[iface].msec_prev = d->ifs[iface].msec;
d->ifs[iface].msec = stb.time*1000+stb.millitm;
delta_t =  ((double)(d->ifs[iface].msec - d->ifs[iface].msec_prev))/1000; // delta t in seconds 

d->ifs[iface].ibytes_prev_prev = d->ifs[iface].ibytes_prev;
d->ifs[iface].obytes_prev_prev = d->ifs[iface].obytes_prev;
d->ifs[iface].ibytes_prev = d->ifs[iface].ibytes;
d->ifs[iface].obytes_prev = d->ifs[iface].obytes;

// we set this values to 0 to take some action below
d->ifs[iface].ibytes = 0;
d->ifs[iface].obytes = 0;

ptr = strtok( b, s2 );    // first
	{
	if ( (p=strstr(ptr, "RxOctetsOK")) !=  NULL ) 
		{
		if( (ptr = strtok( NULL, s2 )) != NULL )    // second
			d->ifs[iface].ibytes = atoll(ptr);
		if( (ptr = strtok( NULL, s2 )) != NULL )    // second
			d->ifs[iface].obytes = atoll(ptr);
		}
	}

if ( d->ifs[iface].ibytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].ibytes_prev > d->ifs[iface].ibytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxin=0;
  
  auxin = (8*(double)(d->ifs[iface].ibytes - d->ifs[iface].ibytes_prev)) / delta_t;
  if (auxin < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].ibw =  auxin;
  
  if ( d->ifs[iface].ibytes_prev_prev == 0 ) // second pass after starting the program, it's a good idea to use current 'instant' traffic as average!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].ibw_buf[j] = d->ifs[iface].ibw;
	d->ifs[iface].ibw_a	= d->ifs[iface].ibw_b = d->ifs[iface].ibw_c = d->ifs[iface].ibw;
	}
  else
  	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].ibw_buf[1]), &(d->ifs[iface].ibw_buf[0]), sizeof((d->ifs[iface].ibw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].ibw_buf[0] = d->ifs[iface].ibw;
	  
	  d->ifs[iface].ibw_a =   0.5 * d->ifs[iface].ibw + 0.5 * d->ifs[iface].ibw_a;
	  d->ifs[iface].ibw_b =   0.1 * d->ifs[iface].ibw + 0.9 * d->ifs[iface].ibw_b;
	  d->ifs[iface].ibw_c =   0.02 * d->ifs[iface].ibw + 0.98 * d->ifs[iface].ibw_c;
	  }
  }

if ( d->ifs[iface].obytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].obytes_prev > d->ifs[iface].obytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxout=0;
  
  auxout = (8*(double)(d->ifs[iface].obytes - d->ifs[iface].obytes_prev)) / delta_t;
  if (auxout < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].obw =  auxout;

  if ( d->ifs[iface].obytes_prev_prev == 0 ) // idem!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].obw_buf[j] = d->ifs[iface].obw;
	d->ifs[iface].obw_a	= d->ifs[iface].obw_b = d->ifs[iface].obw_c = d->ifs[iface].obw;
  }
else
	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].obw_buf[1]), &(d->ifs[iface].obw_buf[0]), sizeof((d->ifs[iface].obw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].obw_buf[0] = d->ifs[iface].obw;

	  d->ifs[iface].obw_a =   0.5 * d->ifs[iface].obw + 0.5 * d->ifs[iface].obw_a;
	  d->ifs[iface].obw_b =   0.1 * d->ifs[iface].obw + 0.9 * d->ifs[iface].obw_b;
	  d->ifs[iface].obw_c =   0.02 * d->ifs[iface].obw + 0.98 * d->ifs[iface].obw_c;
	  }
  }

if (d->ncycle > 10)  // alarms only after startup window
  if (_send_alarm)
	eval_alarm(d, iface);

  
if (_verbose > 1)
  {
  printf("\n\n --------------------------- ");
  printf("\n interface: %s  (%s)", d->ifs[iface].name, d->ifs[iface].description);
  printf("\n delta t:  %lf", delta_t);
  printf("\n ibw: %lf ibw_a: %lf", d->ifs[iface].ibw, d->ifs[iface].ibw_a);
  printf("\n obw: %lf obw_a: %lf", d->ifs[iface].obw, d->ifs[iface].obw_a);
  printf("\n ibytes: %lli obytes: %lli", d->ifs[iface].ibytes, d->ifs[iface].obytes);
  printf("\n ibytes prev: %lli obytes prev: %lli", d->ifs[iface].ibytes_prev, d->ifs[iface].obytes_prev);
  printf("\n --------------------------- \n\n"); 
  fflush(stdout);
  }  

return(0);
}

//------------------------------------------------------------------------

//  for toughswitch
// required command: tswconf mib statistics get PORT

int ubnt_process_ts( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface )
{
	
char 				commandstr[300];
char 				buffer_aux[8000];
int 				comm_error=0;

sprintf(commandstr, "/usr/bin/tswconf mib statistics get %s \n", d->ifs[iface].name);
send_cmd(outfd, commandstr);

if (_verbose > 1)
	{
	printf("\n wait for prompt: |%s| \n ", d->hostname);
	fflush(stdout);
	}

// BW calculations !!!
if ( wait_for_string(infd, 5000, 3, d->hostname, buffer_aux, 2000, 300) )
	{
	if (d->parse_bw)
		d->parse_bw( d, iface, buffer_aux );
	comm_error=0;
	}
else
	comm_error=1;

comm_error=comm_error;
	
return(1);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// this routine parsess UBNT  commands 
// d: device data structure
// n: index of interface to parse
// b: string to parse ('show int' dump)
// toughswitch dums info via command:  tswconf mib statistics get PORT
// datadump: 
// RxGoodByte  <0x257b3bae>  RxGoodByte1 <0x00000000>  RxBadByte   <0x00000000>  
// RxBadByte1  <0x00000000>  RxOverFlow  <0x00000000>  Filtered    <0x0001e6a9>  
// TxBroad     <0x00009245>  TxPause     <0x00000000>  TxMulti     <0x0006127c>  
// TxByte      <0x287b842c>  TxByte1     <0x00000001>  TxCollision <0x00000000>  

int ubnt_parse_bw_ts( device_data *d, int iface, char *b)
{

char 					s2[10] = "\n";
char 					*ptr=NULL, *p=NULL;
long long int   		lli1=0;
struct timeb 			stb;
double					delta_t=0;

ftime(&stb); 
d->ifs[iface].last_access=stb;
d->ifs[iface].msec_prev = d->ifs[iface].msec;
d->ifs[iface].msec = stb.time*1000+stb.millitm;
delta_t =  ((double)(d->ifs[iface].msec - d->ifs[iface].msec_prev))/1000; // delta t in seconds 

d->ifs[iface].ibytes_prev_prev = d->ifs[iface].ibytes_prev;
d->ifs[iface].obytes_prev_prev = d->ifs[iface].obytes_prev;
d->ifs[iface].ibytes_prev = d->ifs[iface].ibytes;
d->ifs[iface].obytes_prev = d->ifs[iface].obytes;

// we set this values to 0 to take some action below
d->ifs[iface].ibytes = 0;
d->ifs[iface].obytes = 0;

ptr = strtok( b, s2 );    // Primera llamada => Primer token
while( (ptr = strtok( NULL, s2 )) != NULL )    // Posteriores llamadas
	{
	// RxGoodByte  <0xaa8c76d4>
	if ( (p=strstr(ptr, "RxGoodByte")) !=  NULL ) 
		{
		sscanf(ptr, "RxGoodByte  <%10llx>", &lli1);
		d->ifs[iface].ibytes = lli1;
		lli1=0;
		}
		
	// TxByte      <0x82cc46d5>
	if ( (p=strstr(ptr, "TxByte")) !=  NULL ) 
		{
		sscanf(ptr, "TxByte      <%10llx>", &lli1);
		d->ifs[iface].obytes = lli1;
		lli1=0;
		}
	}

if ( d->ifs[iface].ibytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].ibytes_prev > d->ifs[iface].ibytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxin=0;
  
  auxin = (8*(double)(d->ifs[iface].ibytes - d->ifs[iface].ibytes_prev)) / delta_t;
  if (auxin < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].ibw =  auxin;
  
  if ( d->ifs[iface].ibytes_prev_prev == 0 ) // second pass after starting the program, it's a good idea to use current 'instant' traffic as average!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].ibw_buf[j] = d->ifs[iface].ibw;
	d->ifs[iface].ibw_a	= d->ifs[iface].ibw_b = d->ifs[iface].ibw_c = d->ifs[iface].ibw;
	}
  else
  	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].ibw_buf[1]), &(d->ifs[iface].ibw_buf[0]), sizeof((d->ifs[iface].ibw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].ibw_buf[0] = d->ifs[iface].ibw;
	  
	  d->ifs[iface].ibw_a =   0.5 * d->ifs[iface].ibw + 0.5 * d->ifs[iface].ibw_a;
	  d->ifs[iface].ibw_b =   0.1 * d->ifs[iface].ibw + 0.9 * d->ifs[iface].ibw_b;
	  d->ifs[iface].ibw_c =   0.02 * d->ifs[iface].ibw + 0.98 * d->ifs[iface].ibw_c;
	  }
  }

if ( d->ifs[iface].obytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].obytes_prev > d->ifs[iface].obytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxout=0;
  
  auxout = (8*(double)(d->ifs[iface].obytes - d->ifs[iface].obytes_prev)) / delta_t;
  if (auxout < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].obw =  auxout;

  if ( d->ifs[iface].obytes_prev_prev == 0 ) // idem!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].obw_buf[j] = d->ifs[iface].obw;
	d->ifs[iface].obw_a	= d->ifs[iface].obw_b = d->ifs[iface].obw_c = d->ifs[iface].obw;
  }
else
	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].obw_buf[1]), &(d->ifs[iface].obw_buf[0]), sizeof((d->ifs[iface].obw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].obw_buf[0] = d->ifs[iface].obw;

	  d->ifs[iface].obw_a =   0.5 * d->ifs[iface].obw + 0.5 * d->ifs[iface].obw_a;
	  d->ifs[iface].obw_b =   0.1 * d->ifs[iface].obw + 0.9 * d->ifs[iface].obw_b;
	  d->ifs[iface].obw_c =   0.02 * d->ifs[iface].obw + 0.98 * d->ifs[iface].obw_c;
	  }
  }

if (d->ncycle > 10)  // alarms only after startup window
  if (_send_alarm)
	eval_alarm(d, iface);

  
if (_verbose > 1)
  {
  printf("\n\n --------------------------- ");
  printf("\n interface: %s  (%s)", d->ifs[iface].name, d->ifs[iface].description);
  printf("\n delta t:  %lf", delta_t);
  printf("\n ibw: %lf ibw_a: %lf", d->ifs[iface].ibw, d->ifs[iface].ibw_a);
  printf("\n obw: %lf obw_a: %lf", d->ifs[iface].obw, d->ifs[iface].obw_a);
  printf("\n ibytes: %lli obytes: %lli", d->ifs[iface].ibytes, d->ifs[iface].obytes);
  printf("\n ibytes prev: %lli obytes prev: %lli", d->ifs[iface].ibytes_prev, d->ifs[iface].obytes_prev);
  printf("\n --------------------------- \n\n"); 
  fflush(stdout);
  }  

return(0);
}



//------------------------------------------------------------------------

//  for edge switch
// required command: show interface counters 

int ubnt_process_es( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface )
{
	
char 				commandstr[300];
char 				buffer_aux[8000];
int 				comm_error=0;

sprintf(commandstr, "show interface counters\n");
send_cmd(outfd, commandstr);

if (_verbose > 1)
	{
	printf("\n wait for prompt: |%s| \n ", d->hostname);
	fflush(stdout);
	}

// BW calculations !!!
if ( wait_for_string(infd, 5000, 3, d->hostname, buffer_aux, 7990, 300) )
	{
	if (d->parse_bw)
		d->parse_bw( d, iface, buffer_aux );
	comm_error=0;
	}
else
	comm_error=1;

comm_error=comm_error;
	
return(1);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// this routine parsess UBNT  commands 
// d: device data structure
// n: index of interface to parse
// b: string to parse ('show interf counters' dump)
// edge switch dums info via command:  show interface counters 
// datadump: 

// Port              InOctets      InUcastPkts      InMcastPkts      InBcastPkts
// --------- ---------------- ---------------- ---------------- ----------------
// 0/1            22860226725         18210502           332536            32160
// 0/2                3045557            12452              664               47
// 0/3                      0                0                0                0
// 0/4             1229861636          7465684             3764             4839
// 0/5               21117202            83964            32665            62702

int ubnt_parse_bw_es( device_data *d, int iface, char *b)
{

char 					s2[10] = "\n\r";
char 					*ptr=NULL, *p=NULL;
long long int   		lli1=0;

struct timeb 			stb;
double					delta_t=0;
int						readingOutOctets=0;

ftime(&stb); 
d->ifs[iface].last_access=stb;
d->ifs[iface].msec_prev = d->ifs[iface].msec;
d->ifs[iface].msec = stb.time*1000+stb.millitm;
delta_t =  ((double)(d->ifs[iface].msec - d->ifs[iface].msec_prev))/1000; // delta t in seconds 

d->ifs[iface].ibytes_prev_prev = d->ifs[iface].ibytes_prev;
d->ifs[iface].obytes_prev_prev = d->ifs[iface].obytes_prev;
d->ifs[iface].ibytes_prev = d->ifs[iface].ibytes;
d->ifs[iface].obytes_prev = d->ifs[iface].obytes;

// we set this values to 0 to take some action below
d->ifs[iface].ibytes = 0;
d->ifs[iface].obytes = 0;

ptr = strtok( b, s2 );    // Primera llamada => Primer token
while( (ptr = strtok( NULL, s2 )) != NULL )    // Posteriores llamadas
	{
	// first block shows Input counters,  the second shows Output
	if ( (p=strstr(ptr, "OutOctets")) != NULL )
		readingOutOctets=1;		

	// find lines starting with INTERFACE Name, followed by SPACE
	if (  ( strncmp(ptr, d->ifs[iface].name, strlen(d->ifs[iface].name)) ) == 0 ) 
		if ( ptr[ strlen(d->ifs[iface].name) ] == ' ' )
			{
			sscanf(ptr, "%*s %lli", &lli1);
		
			if (readingOutOctets) // output values block
				d->ifs[iface].obytes = lli1;
			else               // input values (first block)
				d->ifs[iface].ibytes = lli1;
			}
	}

if ( d->ifs[iface].ibytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].ibytes_prev > d->ifs[iface].ibytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxin=0;
  
  auxin = (8*(double)(d->ifs[iface].ibytes - d->ifs[iface].ibytes_prev)) / delta_t;
  if (auxin < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].ibw =  auxin;
  
  if ( d->ifs[iface].ibytes_prev_prev == 0 ) // second pass after starting the program, it's a good idea to use current 'instant' traffic as average!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].ibw_buf[j] = d->ifs[iface].ibw;
	d->ifs[iface].ibw_a	= d->ifs[iface].ibw_b = d->ifs[iface].ibw_c = d->ifs[iface].ibw;
	}
  else
  	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].ibw_buf[1]), &(d->ifs[iface].ibw_buf[0]), sizeof((d->ifs[iface].ibw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].ibw_buf[0] = d->ifs[iface].ibw;
	  
	  d->ifs[iface].ibw_a =   0.5 * d->ifs[iface].ibw + 0.5 * d->ifs[iface].ibw_a;
	  d->ifs[iface].ibw_b =   0.1 * d->ifs[iface].ibw + 0.9 * d->ifs[iface].ibw_b;
	  d->ifs[iface].ibw_c =   0.02 * d->ifs[iface].ibw + 0.98 * d->ifs[iface].ibw_c;
	  }
  }

if ( d->ifs[iface].obytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].obytes_prev > d->ifs[iface].obytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxout=0;
  
  auxout = (8*(double)(d->ifs[iface].obytes - d->ifs[iface].obytes_prev)) / delta_t;
  if (auxout < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].obw =  auxout;

  if ( d->ifs[iface].obytes_prev_prev == 0 ) // idem!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].obw_buf[j] = d->ifs[iface].obw;
	d->ifs[iface].obw_a	= d->ifs[iface].obw_b = d->ifs[iface].obw_c = d->ifs[iface].obw;
  }
else
	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].obw_buf[1]), &(d->ifs[iface].obw_buf[0]), sizeof((d->ifs[iface].obw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].obw_buf[0] = d->ifs[iface].obw;

	  d->ifs[iface].obw_a =   0.5 * d->ifs[iface].obw + 0.5 * d->ifs[iface].obw_a;
	  d->ifs[iface].obw_b =   0.1 * d->ifs[iface].obw + 0.9 * d->ifs[iface].obw_b;
	  d->ifs[iface].obw_c =   0.02 * d->ifs[iface].obw + 0.98 * d->ifs[iface].obw_c;
	  }
  }

if (d->ncycle > 10)  // alarms only after startup window
  if (_send_alarm)
	eval_alarm(d, iface);

  
if (_verbose > 1)
  {
  printf("\n\n --------------------------- ");
  printf("\n interface: %s  (%s)", d->ifs[iface].name, d->ifs[iface].description);
  printf("\n delta t:  %lf", delta_t);
  printf("\n ibw: %lf ibw_a: %lf", d->ifs[iface].ibw, d->ifs[iface].ibw_a);
  printf("\n obw: %lf obw_a: %lf", d->ifs[iface].obw, d->ifs[iface].obw_a);
  printf("\n ibytes: %lli obytes: %lli", d->ifs[iface].ibytes, d->ifs[iface].obytes);
  printf("\n ibytes prev: %lli obytes prev: %lli", d->ifs[iface].ibytes_prev, d->ifs[iface].obytes_prev);
  printf("\n --------------------------- \n\n"); 
  fflush(stdout);
  }  

return(0);
}



//------------------------------------------------------------------------

//  for Ubnt devices that support "ubntbox mca-status"  and dump JSON data 

int ubnt_process_mca( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface )
{
	
char 				commandstr[300];
char 				buffer_aux[20000];
int 				comm_error=0;

//sprintf(commandstr, "ubntbox mca-status \n", d->ifs[iface].name);
sprintf(commandstr, "ubntbox mca-status \n");

send_cmd(outfd, commandstr);

if (_verbose > 1)
	{
	printf("\n wait for prompt: |%s| \n ", d->hostname);
	fflush(stdout);
	}

// BW calculations !!!
if ( wait_for_string(infd, 5000, 3, d->hostname, buffer_aux, 18000, 300) )
	{
	if (d->parse_bw)
		d->parse_bw( d, iface, buffer_aux );
	comm_error=0;
	}
else
	comm_error=1;

comm_error=comm_error;
	
return(1);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// this routine parsess UBNT  commands 
// d: device data structure
// n: index of interface to parse
// b: string to parse ('show int' dump)
// toughswitch dums info via command:  ubntbox mca-status
// datadump: 

//"ports": [
//	{
//	"index": 1, "status": 1, "link_status": 1, "speed": 100, "duplex": 1,
//	"mtu": 1518, "stp": "Forwarding", "poe": 0,
//	"stats": {
//		"rx_bytes": 2127994, "rx_packets": 27042, "rx_errors": 0,
//		"tx_bytes": 102007882, "tx_packets": 1211651, "tx_errors": 0
//	  	     }
//	},
																	

int ubnt_parse_bw_mca( device_data *d, int iface, char *b)
{

char 					s2[10] = "\n";
char 					*ptr=NULL, *p=NULL;
long long int   		lli1=0;
struct timeb 			stb;
double					delta_t=0;
char 					token[300];
int 					interfaceFound=0, rxLoaded=0, txLoaded=0;

ftime(&stb); 
d->ifs[iface].last_access=stb;
d->ifs[iface].msec_prev = d->ifs[iface].msec;
d->ifs[iface].msec = stb.time*1000+stb.millitm;
delta_t =  ((double)(d->ifs[iface].msec - d->ifs[iface].msec_prev))/1000; // delta t in seconds 

d->ifs[iface].ibytes_prev_prev = d->ifs[iface].ibytes_prev;
d->ifs[iface].obytes_prev_prev = d->ifs[iface].obytes_prev;
d->ifs[iface].ibytes_prev = d->ifs[iface].ibytes;
d->ifs[iface].obytes_prev = d->ifs[iface].obytes;

// we set this values to 0 to take some action below
d->ifs[iface].ibytes = 0;
d->ifs[iface].obytes = 0;

ptr = strtok( b, s2 );    // Primera llamada => Primer token
while( (ptr = strtok( NULL, s2 )) != NULL )    // Posteriores llamadas
	{
	sprintf(token, "\"index\": %s", d->ifs[iface].name)	;
	if ( (p=strstr(ptr, token)) !=  NULL )               	// search for interface index 
		{
		printf("\n FOUND: |%s|", ptr);
		interfaceFound=1;
		rxLoaded=0, txLoaded=0;
		}

	if 	(interfaceFound == 1)	// once we've found interface index, search for RX and TX counters
		{		
		// RxByte 
		if ( (p=strstr(ptr, "\"rx_bytes\"")) !=  NULL ) 
			{
			sscanf(p, "\"rx_bytes\": %lli", &lli1);
			rxLoaded=1;
			d->ifs[iface].ibytes = lli1;
			lli1=0;
			}

		if ( (p=strstr(ptr, "\"tx_bytes\"")) !=  NULL ) 
			{
			sscanf(p, "\"tx_bytes\": %lli", &lli1);
			txLoaded=1;
			d->ifs[iface].obytes = lli1;
			lli1=0;
			}			
		}	
	if 	(interfaceFound==1 && txLoaded==1 && rxLoaded==1)	// all done, quit searchuing!
		break;		
	}

if ( d->ifs[iface].ibytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].ibytes_prev > d->ifs[iface].ibytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxin=0;
  
  auxin = (8*(double)(d->ifs[iface].ibytes - d->ifs[iface].ibytes_prev)) / delta_t;
  if (auxin < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].ibw =  auxin;
  
  if ( d->ifs[iface].ibytes_prev_prev == 0 ) // second pass after starting the program, it's a good idea to use current 'instant' traffic as average!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].ibw_buf[j] = d->ifs[iface].ibw;
	d->ifs[iface].ibw_a	= d->ifs[iface].ibw_b = d->ifs[iface].ibw_c = d->ifs[iface].ibw;
	}
  else
  	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].ibw_buf[1]), &(d->ifs[iface].ibw_buf[0]), sizeof((d->ifs[iface].ibw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].ibw_buf[0] = d->ifs[iface].ibw;
	  
	  d->ifs[iface].ibw_a =   0.5 * d->ifs[iface].ibw + 0.5 * d->ifs[iface].ibw_a;
	  d->ifs[iface].ibw_b =   0.1 * d->ifs[iface].ibw + 0.9 * d->ifs[iface].ibw_b;
	  d->ifs[iface].ibw_c =   0.02 * d->ifs[iface].ibw + 0.98 * d->ifs[iface].ibw_c;
	  }
  }

if ( d->ifs[iface].obytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].obytes_prev > d->ifs[iface].obytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxout=0;
  
  auxout = (8*(double)(d->ifs[iface].obytes - d->ifs[iface].obytes_prev)) / delta_t;
  if (auxout < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].obw =  auxout;

  if ( d->ifs[iface].obytes_prev_prev == 0 ) // idem!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].obw_buf[j] = d->ifs[iface].obw;
	d->ifs[iface].obw_a	= d->ifs[iface].obw_b = d->ifs[iface].obw_c = d->ifs[iface].obw;
  }
else
	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].obw_buf[1]), &(d->ifs[iface].obw_buf[0]), sizeof((d->ifs[iface].obw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].obw_buf[0] = d->ifs[iface].obw;

	  d->ifs[iface].obw_a =   0.5 * d->ifs[iface].obw + 0.5 * d->ifs[iface].obw_a;
	  d->ifs[iface].obw_b =   0.1 * d->ifs[iface].obw + 0.9 * d->ifs[iface].obw_b;
	  d->ifs[iface].obw_c =   0.02 * d->ifs[iface].obw + 0.98 * d->ifs[iface].obw_c;
	  }
  }

if (d->ncycle > 10)  // alarms only after startup window
  if (_send_alarm)
	eval_alarm(d, iface);

  
if (_verbose > 1)
  {
  printf("\n\n --------------------------- ");
  printf("\n interface: %s  (%s)", d->ifs[iface].name, d->ifs[iface].description);
  printf("\n delta t:  %lf", delta_t);
  printf("\n ibw: %lf ibw_a: %lf", d->ifs[iface].ibw, d->ifs[iface].ibw_a);
  printf("\n obw: %lf obw_a: %lf", d->ifs[iface].obw, d->ifs[iface].obw_a);
  printf("\n ibytes: %lli obytes: %lli", d->ifs[iface].ibytes, d->ifs[iface].obytes);
  printf("\n ibytes prev: %lli obytes prev: %lli", d->ifs[iface].ibytes_prev, d->ifs[iface].obytes_prev);
  printf("\n --------------------------- \n\n"); 
  fflush(stdout);
  }  

return(0);
}


//------------------------------------------------------------------------

int ubnt_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface )
{
	
char 				commandstr[300];
char 				buffer_aux[8000];
int 				comm_error=0;

sprintf(commandstr, "ifconfig %s \n", d->ifs[iface].name);
send_cmd(outfd, commandstr);

if (_verbose > 1)
	{
	printf("\n wait for prompt: |%s| \n ", d->hostname);
	fflush(stdout);
	}

// BW calculations !!!
if ( wait_for_string(infd, 5000, 3, d->hostname, buffer_aux, 2000, 300) )
	{
	if (d->parse_bw)
		d->parse_bw( d, iface, buffer_aux );
	comm_error=0;
	}
else
	comm_error=1;


comm_error = comm_error;	
return(1);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
// this routine parses 'show int counters (Allied Telesis)' command, 
// parsing a line like .....
// d: device data structure
// n: index of interface to parse
// b: string to parse ('show int' dump)

int ubnt_parse_bw( device_data *d, int iface, char *b)
{

char 					straux[300];
char 					s2[10] = "\n";
char 					*ptr=NULL, *p=NULL;
//int   					i1=0;
struct timeb 			stb;
double					delta_t=0;
//int                     pass=0;			// 0: input, 1: output	


ftime(&stb); 
d->ifs[iface].last_access=stb;
d->ifs[iface].msec_prev = d->ifs[iface].msec;
d->ifs[iface].msec = stb.time*1000+stb.millitm;
delta_t =  ((double)(d->ifs[iface].msec - d->ifs[iface].msec_prev))/1000; // delta t in seconds 

d->ifs[iface].ibytes_prev_prev = d->ifs[iface].ibytes_prev;
d->ifs[iface].obytes_prev_prev = d->ifs[iface].obytes_prev;
d->ifs[iface].ibytes_prev = d->ifs[iface].ibytes;
d->ifs[iface].obytes_prev = d->ifs[iface].obytes;

// we set this values to 0 to take some action below
d->ifs[iface].ibytes = 0;
d->ifs[iface].obytes = 0;

ptr = strtok( b, s2 );    // Primera llamada => Primer token
while( (ptr = strtok( NULL, s2 )) != NULL )    // Posteriores llamadas
	{
	if ( (p=strstr(ptr, "RX bytes:")) !=  NULL ) 
		{
		int j=0;
		// We need to get RX and TX bytes from a string like this: "RX bytes:1205491646 (1.1 GiB)  TX bytes:71368917 (68.0 MiB)"
		// first tep: move forward to ':' 
		while ( p && (*p != ':') )
			p++;	

		// now we copy the numbers until first whitespace!
		p++;  // to skip ':' just found
		while ( p && (*p != ' ' && *p != '(' ) )
			straux[j++] = *(p++);
		straux[j] = 0;
		
		d->ifs[iface].ibytes = atoll(straux);
		}
	if ( (p=strstr(ptr, "TX bytes:")) !=  NULL ) 
		{
		int j=0;
		// idem RX bytes	
		while ( p && (*p != ':') )			
			p++;	

		p++;  // to skip ':' just found
		while ( *p && (*p != ' ' && *p != '(' ) )
			straux[j++] = *(p++);
		straux[j] = 0;
		
		d->ifs[iface].obytes = atoll(straux);
		}
	}

if ( d->ifs[iface].ibytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].ibytes_prev > d->ifs[iface].ibytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
//  double ibw_prev=d->ifs[iface].ibw;
  double auxin=0;
  
  auxin = (8*(double)(d->ifs[iface].ibytes - d->ifs[iface].ibytes_prev)) / delta_t;
  if (auxin < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].ibw =  auxin;
  
  if ( d->ifs[iface].ibytes_prev_prev == 0 ) // second pass after starting the program, it's a good idea to use current 'instant' traffic as average!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].ibw_buf[j] = d->ifs[iface].ibw;
	d->ifs[iface].ibw_a	= d->ifs[iface].ibw_b = d->ifs[iface].ibw_c = d->ifs[iface].ibw;
	}
  else
  	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].ibw_buf[1]), &(d->ifs[iface].ibw_buf[0]), sizeof((d->ifs[iface].ibw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].ibw_buf[0] = d->ifs[iface].ibw;
	  
	  d->ifs[iface].ibw_a =   0.5 * d->ifs[iface].ibw + 0.5 * d->ifs[iface].ibw_a;
	  d->ifs[iface].ibw_b =   0.1 * d->ifs[iface].ibw + 0.9 * d->ifs[iface].ibw_b;
	  d->ifs[iface].ibw_c =   0.02 * d->ifs[iface].ibw + 0.98 * d->ifs[iface].ibw_c;
	  }
  }

if ( d->ifs[iface].obytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[iface].obytes_prev > d->ifs[iface].obytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  //double obw_prev=d->ifs[iface].obw;
  double auxout=0;
  
  auxout = (8*(double)(d->ifs[iface].obytes - d->ifs[iface].obytes_prev)) / delta_t;
  if (auxout < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[iface].obw =  auxout;

  if ( d->ifs[iface].obytes_prev_prev == 0 ) // idem!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[iface].obw_buf[j] = d->ifs[iface].obw;
	d->ifs[iface].obw_a	= d->ifs[iface].obw_b = d->ifs[iface].obw_c = d->ifs[iface].obw;
  }
else
	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[iface].obw_buf[1]), &(d->ifs[iface].obw_buf[0]), sizeof((d->ifs[iface].obw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[iface].obw_buf[0] = d->ifs[iface].obw;

	  d->ifs[iface].obw_a =   0.5 * d->ifs[iface].obw + 0.5 * d->ifs[iface].obw_a;
	  d->ifs[iface].obw_b =   0.1 * d->ifs[iface].obw + 0.9 * d->ifs[iface].obw_b;
	  d->ifs[iface].obw_c =   0.02 * d->ifs[iface].obw + 0.98 * d->ifs[iface].obw_c;
	  }
  }

if (d->ncycle > 10)  // alarms only after startup window
  if (_send_alarm)
	eval_alarm(d, iface);

  
if (_verbose > 1)
  {
  printf("\n\n --------------------------- ");
  printf("\n interface: %s  (%s)", d->ifs[iface].name, d->ifs[iface].description);
  printf("\n delta t:  %lf", delta_t);
  printf("\n ibw: %lf ibw_a: %lf", d->ifs[iface].ibw, d->ifs[iface].ibw_a);
  printf("\n obw: %lf obw_a: %lf", d->ifs[iface].obw, d->ifs[iface].obw_a);
  printf("\n ibytes: %lli obytes: %lli", d->ifs[iface].ibytes, d->ifs[iface].obytes);
  printf("\n ibytes prev: %lli obytes prev: %lli", d->ifs[iface].ibytes_prev, d->ifs[iface].obytes_prev);
  printf("\n --------------------------- \n\n"); 
  fflush(stdout);
  }  

	
return(0);
}


//------------------------------------------------------------------------

int ubnt_disconnect( device_data *d, int infd, int outfd )
{
send_cmd(outfd, "exit\n");
send_cmd(outfd, "exit\n");

if (_verbose > 4)
  printf("\n Disconnection request SENT (exit) \n");
  
fflush(stdout);  
return(1);
}

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------
