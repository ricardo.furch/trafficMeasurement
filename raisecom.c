/* 
  Author: rfurch
 
  TODO:
*/


#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <time.h>
#include <pty.h>
#include <sys/time.h>
#include <sys/select.h>
#include <stdio.h>
#include <ctype.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <semaphore.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <signal.h>
#include <pty.h>
#include <math.h>

#include <sys/timeb.h>

#include "ifaceData.h"                                                                                                     

extern int             _verbose;
extern pid_t           _child_pid;
extern pv_list         _pvl;
extern int             _to_epics;
extern int             _to_files;
extern int             _to_memdb;
extern int             _to_hisdb;
extern int             _send_alarm;
extern char            _process_name[];

extern char     		server[];


//------------------------------------------------------------------------

// authentication handling...

int 	raisecom_authenticate( device_data *d, int infd, int outfd )
{
char buffer_aux[6000];

if (d->cli_acc == CLI_ACCESS_SSH)
  {
  // if access type is ssh, we might need to send 'yes' to add site to the known hosts list
  if (  wait_for_string(infd, 3000, 3, "(yes/no)", buffer_aux, 2000, 300 ) )
	send_cmd(outfd, "yes\n");

  if (strstr(buffer_aux, "sword:"))	
	{
	send_cmd(outfd, "nUevo1983\n");
	if ( ! wait_for_string(infd, 5000, 3, ">", buffer_aux, 2000, 300) )
	  {
	  printf("\n\n Authentication error 1 \n\n ");
	  fflush(stdout);
	  return(0);
	  }
	}
  else
	{
	if ( ! wait_for_string(infd, 5000, 3, "word", buffer_aux, 2000, 300) )
	  {
	  printf("\n\n Authentication error 2 \n\n ");
	  fflush(stdout);
	  return(0);
	  }

	send_cmd(outfd, "nUevo1983\n");
	if ( ! wait_for_string(infd, 5000, 3, ">", buffer_aux, 2000, 300) )
	  {
	  printf("\n\n Authentication error 3 \n\n ");
	  fflush(stdout);
	  return(0);
	  }
	}	  

  usleep(500000);
  }
else if (d->cli_acc == CLI_ACCESS_TELNET)
  {
  char 	straux[300];

	sprintf(straux, "open  %s\n", d->ip);
	send_cmd(outfd, straux);
	if ( ! wait_for_string_nonzero(infd, 3000, 5, "Login:", buffer_aux, 5000, 300 ) )
	{
	printf("\n\n Authentication error 4.1 \n\n ");
	fflush(stdout);
	return(0);
	}

	send_cmd(outfd, "adc\n");
	if ( ! wait_for_string_nonzero(infd, 5000, 3, "word", buffer_aux, 2000, 300) )
	{
	printf("\n\n Authentication error 5 \n\n ");
	fflush(stdout);
	return(0);
	}

	send_cmd(outfd, "nUevo1983\n");
	if ( ! wait_for_string_nonzero(infd, 5000, 3, "#", buffer_aux, 2000, 300) )
	{
	printf("\n\n Authentication error 6 \n\n ");
	fflush(stdout);
	return(0);
	}
  }

send_cmd(outfd, "\n");
if ( ! wait_for_string_nonzero(infd, 5000, 3, "#", buffer_aux, 2000, 300) )
  {
  printf("\n\n Authentication error 7 \n\n ");
  fflush(stdout);
  return(0);
  }

get_hostname(buffer_aux, d->hostname);
strcpy(d->ena_prompt, d->hostname);
strcpy(d->dis_prompt, d->hostname);
strcat(d->ena_prompt, "#");
strcat(d->dis_prompt, "#");

send_cmd(outfd, "terminal page-break disable\n");
wait_for_string(infd, 5000, 3, d->dis_prompt, buffer_aux, 2000, 300);

return(1);
}

//------------------------------------------------------------------------


// this routine parses 'show interface line 1 statistics' raisecom command, 
// parsing a line like this :
// Traffic statistics:
//                                        
// InOctets:                         1,449,159,176,406             

// d: device data structure
// n: index of interface to parse
// b: string to parse ('show int' dump)

int raisecom_parse_bw( device_data *d, int n, char *b)
{
char 					straux[300];
char 					s2[10] = "\n";
char 					*ptr=NULL;
struct timeb 			stb;
double					delta_t=0;

ftime(&stb); 
d->ifs[n].last_access=stb;
d->ifs[n].msec_prev = d->ifs[n].msec;
d->ifs[n].msec = stb.time*1000+stb.millitm;
delta_t =  ((double)(d->ifs[n].msec - d->ifs[n].msec_prev))/1000; // delta t in seconds 

d->ifs[n].ibytes_prev_prev = d->ifs[n].ibytes_prev;
d->ifs[n].obytes_prev_prev = d->ifs[n].obytes_prev;
d->ifs[n].ibytes_prev = d->ifs[n].ibytes;
d->ifs[n].obytes_prev = d->ifs[n].obytes;

// we set this values to 0 to take some action below
d->ifs[n].ibytes = 0;
d->ifs[n].obytes = 0;

ptr = strtok( b, s2 );    // Primera llamada => Primer token
while( (ptr = strtok( NULL, s2 )) != NULL )    // Posteriores llamadas
  {
  if (strstr(ptr, "InOctets")) 
	{
	int i=0, j=0;


	while ( *(ptr+i) != ':' && i< 200 )	// discard until ':'
		i++;

	while ( ((*(ptr+i) < '0') || (*(ptr+i) > '9'))  && i< 200 )	// discard until first numeric digit
		i++;

    j=0;
	while ( ((*(ptr+i) >= '0') || (*(ptr+i) <= '9') || (*(ptr+i) == ','))  && i< 200 )	// copy numeri chars
		{
        if ( (*(ptr+i) != ',') )
			straux[j++] = *(ptr + i);
        i++;
		}

	straux[j++] = 0;
	d->ifs[n].ibytes = atoll(straux);
    }
  else if (strstr(ptr, "OutOctets")) 
	{
	int i=0, j=0;

	while ( *(ptr+i) != ':' && i< 200 )	// discard until ':'
		i++;

	while ( ((*(ptr+i) < '0') || (*(ptr+i) > '9'))  && i< 200 )	// discard until first numeric digit
		i++;

    j=0;
	while ( ((*(ptr+i) >= '0') || (*(ptr+i) <= '9') || (*(ptr+i) == ','))  && i< 200 )	// copy numeri chars
        {
		if ( (*(ptr+i) != ',') )
			straux[j++] = *(ptr + i);
		i++;
		}

	straux[j++] = 0;
	d->ifs[n].obytes = atoll(straux);

	// as 'show interface extensive' command exposes several counters and we need only the first, the
	// parsing is stopped here with a 'break' 
	break;
    }
  else if (strstr(ptr, "Description:")) 	// parse description line....
	{
	int k=0;
	char *p=strstr(ptr, "Description:");
	p+=12;
	while ( (*p == ' ' || *p == ':' ) && *p )
	  p++;
	while (*p != '\n' && *p!= '\r' && *p)
	  d->ifs[n].description[k++] = *(p++);
	d->ifs[n].description[k] = 0;
	}    
  }

if ( d->ifs[n].ibytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[n].ibytes_prev > d->ifs[n].ibytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxin=0;
  
  auxin = (8*(double)(d->ifs[n].ibytes - d->ifs[n].ibytes_prev)) / delta_t;
  if (auxin < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[n].ibw =  auxin;
  
  if ( d->ifs[n].ibytes_prev_prev == 0 ) // second pass after starting the program, it's a good idea to use current 'instant' traffic as average!
	{	
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[n].ibw_buf[j] = d->ifs[n].ibw;
	d->ifs[n].ibw_a	= d->ifs[n].ibw_b = d->ifs[n].ibw_c = d->ifs[n].ibw;
	}
  else
  	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[n].ibw_buf[1]), &(d->ifs[n].ibw_buf[0]), sizeof((d->ifs[n].ibw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[n].ibw_buf[0] = d->ifs[n].ibw;
	  
	  d->ifs[n].ibw_a =   0.5 * d->ifs[n].ibw + 0.5 * d->ifs[n].ibw_a;
	  d->ifs[n].ibw_b =   0.1 * d->ifs[n].ibw + 0.9 * d->ifs[n].ibw_b;
	  d->ifs[n].ibw_c =   0.02 * d->ifs[n].ibw + 0.98 * d->ifs[n].ibw_c;
	  }
  }

if ( d->ifs[n].obytes_prev == 0 ) // just starting, don't do anything
  {
  }
else if ( d->ifs[n].obytes_prev > d->ifs[n].obytes ) // prev > current, there was a clear counter, counters return to 0, etc, don't do eny calculations
  {
  }
else
  {
  double auxout=0;
  
  auxout = (8*(double)(d->ifs[n].obytes - d->ifs[n].obytes_prev)) / delta_t;
  if (auxout < (((long long int)10) * 1000 * 1000 * 1000))  // < 10Gbps 
	d->ifs[n].obw =  auxout;

  if ( d->ifs[n].obytes_prev_prev == 0 ) // idem!
	{
	int j=0;
	for (j=0 ; j<MAXAVGBUF ; j++)	// copy FIRST sample to the WHOLE buffer
		d->ifs[n].obw_buf[j] = d->ifs[n].obw;
	d->ifs[n].obw_a	= d->ifs[n].obw_b = d->ifs[n].obw_c = d->ifs[n].obw;
  }
else
	  {
	  // shift and copy AVG buffer.  Last value is always in the first position, previous in the second an so on...
	  memmove( &(d->ifs[n].obw_buf[1]), &(d->ifs[n].obw_buf[0]), sizeof((d->ifs[n].obw_buf[0])) * (MAXAVGBUF - 1) );  
	  d->ifs[n].obw_buf[0] = d->ifs[n].obw;

	  d->ifs[n].obw_a =   0.5 * d->ifs[n].obw + 0.5 * d->ifs[n].obw_a;
	  d->ifs[n].obw_b =   0.1 * d->ifs[n].obw + 0.9 * d->ifs[n].obw_b;
	  d->ifs[n].obw_c =   0.02 * d->ifs[n].obw + 0.98 * d->ifs[n].obw_c;
	  }
  }

if (d->ncycle > 10)  // alarms only after startup window
  if (_send_alarm)
	eval_alarm(d, n);

  
if (_verbose > 1)
  {
  printf("\n\n --------------------------- ");
  printf("\n interface: %s  (%s)", d->ifs[n].name, d->ifs[n].description);
  printf("\n delta t:  %lf", delta_t);
  printf("\n ibw: %lf ibw_a: %lf", d->ifs[n].ibw, d->ifs[n].ibw_a);
  printf("\n obw: %lf obw_a: %lf", d->ifs[n].obw, d->ifs[n].obw_a);
  printf("\n ibytes: %lli obytes: %lli", d->ifs[n].ibytes, d->ifs[n].obytes);
  printf("\n ibytes prev: %lli obytes prev: %lli", d->ifs[n].ibytes_prev, d->ifs[n].obytes_prev);
  printf("\n --------------------------- \n\n"); 
  fflush(stdout);
  }  

return(0);
}

//------------------------------------------------------------------------

int raisecom_process( device_data *d, int infd, int outfd, pid_t child_pid, int dev_id,int iface )
{
	
char 				commandstr[300];
char 				buffer_aux[10000];
int 				comm_error=0;

sprintf(commandstr, "show interface %s statistics\n", d->ifs[iface].name);
send_cmd(outfd, commandstr);

if (_verbose > 4)
	{
	printf("\n wait for prompt: |%s| \n ", d->dis_prompt);
	fflush(stdout);
	}
// BW calculations !!!
if ( wait_for_string(infd, 5000, 3, d->dis_prompt, buffer_aux, 8000, 300) )
	{
	if (d->parse_bw)
		d->parse_bw( d, iface, buffer_aux );		
	comm_error=0;
	}
else
	comm_error=1;

comm_error = comm_error;

return(1);
}

//------------------------------------------------------------------------

int raisecom_disconnect( device_data *d, int infd, int outfd )
{
send_cmd(outfd, "exit\n");
send_cmd(outfd, "logout\n");

if (_verbose > 4)
  printf("\n Disconnection request SENT (logout-close-quit) \n");
  
fflush(stdout);  
return(1);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
